// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload')
// console.log(payloadCookie)
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
  const storedValue = payloadCookie.value
//   console.log(storedValue)
//   const encodedPayload = JSON.parse(storedValue);
//   console.log(encodedPayload)
  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(storedValue);

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);

  // Print the payload
  console.log(payload);

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (payload.user.perms.includes("events.add_conference")) {
    const selectConf = document.getElementById('add-conf')
    selectConf.classList.remove('d-none')
  }

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (payload.user.perms.includes("events.add_location")) {
    const selectLocation = document.getElementById('add-loc')
    selectLocation.classList.remove('d-none')
  }
}
