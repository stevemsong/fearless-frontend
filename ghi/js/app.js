function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card p-0 border-0 shadow mb-3 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          ${startDate} - ${endDate}
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    let colSelect = 1
    try {
        const response = await fetch(url);

        if (!response.ok) {
          // Figure out what to do when the response is bad
          return `
            <div class="alert alert-info" role="alert">There was a bad response!</div>
          `
        } else {
          const data = await response.json();

          for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const startDate = new Date(details.conference.starts).toLocaleDateString("en-US");
              const endDate = new Date(details.conference.ends).toLocaleDateString("en-US");
              const location = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, startDate, endDate, location);
              const column = document.querySelector(`.col${colSelect}`);
              column.innerHTML += html;
              colSelect += 1
              if (colSelect === 4) {
                colSelect = 1
              }
              }
          }
        }
      } catch (e) {
        // Figure out what to do if an error is raised
            return `
            <div class="alert alert-info" role="alert">There was an error!</div>
        `
      }
});
